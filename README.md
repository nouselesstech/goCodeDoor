# goCodeDoor

## Overview

This is a proof of concept designed to show how unmitigated, winget can be weaponized to create a backdoor on Windows endpoints.

## Prerequisites

- Github Account
- Golang installed on labmachine (executable not created on purpose)
- Microsoft incoming web hook: https://learn.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook?tabs=newteams%2Cdotnet

## Setup

1. Open cmd/main.go and update the teamsUri variable with your webhook.
1. Move `main.go` to your victim machine.

## Running the exploit

1. Make sure your teams channel is open.
1. Run the exploit: `go run ./main.go exploit`
1. Wait for the teams channel to show the authentication code
1. Navigate to https://github.com/login/device
1. Enter the code and authenticate to GitHub
1. Wait until the teams channel updates with the URL
1. Open the URL in your attack machine
1. Profit.
